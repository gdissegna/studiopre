<html>
<head>
    <title>App Name - @yield('title')</title>
    <meta charset="UTF-8">
    <meta content="text/html">
    @include('admin.partials.css')

</head>
<body>

<div class="navbar navbar-inverse " role="navigation">
    <div class="navbar-header">
        <div class="logo"><h1>Dashboard - StudioPre CMS</h1></div>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
</div>

<div class="template-page-wrapper clearfix">

    @include('admin.partials.sidebar')

    <div class="templatemo-content-wrapper">
        <div class="templatemo-content">
            @yield('content')
        </div>
    </div>
    </div>
    </div>
    <footer class="templatemo-footer">
        <div class="templatemo-copyright">
            <p>Copyright &copy; 2015 Omega <!-- Credit: www.templatemo.com --></p>
        </div>
    </footer>
        @include('admin.partials.js')

</body>
</html>