<?php

namespace App\Http\Controllers;

use App\SliderImage;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;

class CategoriesController extends Controller
{
    public function CategoriesPage()
    {
        $categories = Category::with('children','products')->where('parent','0')->get();

        $sliders = SliderImage::all();

        return view('front.categorie')->with('categories', $categories)->with('sliders', $sliders);
    }
}
