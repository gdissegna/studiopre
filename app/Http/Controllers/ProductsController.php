<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\SliderImage;
use Illuminate\Http\Request;
use Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     *
     */
    public function ProductList($slug)
    {
        $menu_categories = Category::with('children')->where('parent', 0)->get();

        $category = Category::findBySlug($slug);

        $products = Product::with('images')->where('Category_id',$category->id )->get();

        $sliders = SliderImage::all();

        return view('front.prodotti')->with('products', $products)
            ->with('menu_categories', $menu_categories)
            ->with('category', $category)
            ->with('sliders', $sliders);

    }

    public function SingleProduct($slug)
    {
        $product = Product::with('download','images')->where('slug', $slug)->first();


        $homeproducts = Product::with('images')->where('inHomepage', 1)->limit(4)->get();


        $categories = Category::with('children')->where('parent','0')->get();

        $sliders = SliderImage::all();

        return view('front.prodotto')
            ->with('product', $product)
            ->with('homeproducts', $homeproducts)
            ->with('categories', $categories)
            ->with('sliders', $sliders);

    }

    public function download($slug) {

        $product = Product::with('download')->where('slug',$slug)->first();

        $file = $product->download->file;

        return Response::download($file);
    }
}
