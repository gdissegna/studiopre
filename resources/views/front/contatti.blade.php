@extends('master')

@section('meta')
    @parent
@endsection

@section('content')

    <div id="content" style="margin-bottom: 25px">
        <div id="breadcrumb"><!-- breadcrumb starts-->
            <div class="container">
                <div class="one-half">
                    <h4>Contatti</h4>
                </div>
                <div class="one-half">
                    <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                        <ul>
                            <li>You are here:</li>
                            <li><a href="/">Home</a></li>
                            <li><a href="/contatti">contatti</a></li>
                        </ul>
                    </nav>
                    <!--breadcrumb nav ends -->
                </div>
            </div>
        </div>
        <!--breadcrumbs ends -->
        <div class="container">
            <div id="google-map">
			</div>
            <div class="one">
                <h1>Contatti</h1>

                <div id="error-field">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('Succes') }}</div>
                @endif
                {!! Form::open(['contatti' => 'contatti','url' => '/contatti', 'method' => 'post','class' => 'simple-form']) !!}

                    <label>Nome*</label>
                    <fieldset>
                    {!! Form::text('nome',  Input::old('nome')  , ['class' => 'form-control']) !!}
                    </fieldset>

                    <label>Cognome*</label>
                <fieldset>
                    {!! Form::text('cognome',  Input::old('cognome') , ['class' => 'form-control']) !!}
                </fieldset>

                    <label>Azienda</label>
                <fieldset>
                    {!! Form::text('azienda',  Input::old('azienda') , ['class' => 'form-control']) !!}
                </fieldset>

                    <label>Email*</label>
                <fieldset>
                    {!! Form::text('email', Input::old('email')  , ['class' => 'form-control']) !!}
                </fieldset>

                    <label>Indirizzo</label>
                <fieldset>
                    {!! Form::text('Indirizzo', Input::old('Indirizzo')  , ['class' => 'form-control']) !!}
                </fieldset>
                <label>Citta</label>
                <fieldset>
                    {!! Form::text('citta', Input::old('citta') , ['class' => 'form-control']) !!}
                </fieldset>
                <label>Provincia</label>
                <fieldset>
                    {!! Form::text('provincia', Input::old('provincia')  , ['class' => 'form-control']) !!}
                </fieldset>
                <label>Sono interessato a:</label>
                <select name="interessi" class="form-control" style="width: 100%;height: 180px;"  multiple>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ (Input::old('name', 0) === $category->id ? ' selected="selected"' : '') }}>{{ $category->name }}</option>
                    @endforeach
                </select>
                <br>
                <label>Messaggio:</label>
                <fieldset>
                {!! Form::textarea('messaggio',  Input::old('messaggio') ,['class' =>'form-control']) !!}
                </fieldset>
                <br>
                {!! Recaptcha::render() !!}
                <br>
                <div style="margin-top: 250px"></div>
                {!! Form::submit('Fatto!', ['class' => 'button huge color-alt']) !!}

                {!! Form::close() !!}
            </div>

        </div>

    </div>


@endsection