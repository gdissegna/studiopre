<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ContactFormRequest;
use App\Jobs\SendContactEmails;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactsFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('front.contatti')->with('categories', $categories);
    }

    /**
     * @param ContactFormRequest $request
     * return view()
     */
    public function submittedform(ContactFormRequest $request) {

        $category = Category::findBySlugOrId($request['interessi']);

        $job = (new SendContactEmails($request,$category));

        $this->dispatch($job);

        $categories = Category::all();

        \Session::flash('success', 'la mail e stata inviata con successo');

        return view('front.contatti')->with('categories', $categories);
    }
}
