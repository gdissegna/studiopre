<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Product extends Model implements SluggableInterface {

	use SluggableTrait;

    protected $table = "products";

    protected $fillable = [];
    protected $guarded = [];

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug',
    );



    public function Category() {

       return $this->belongsTo('App\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Images() {
        return $this->hasMany('App\ProductImage','product_id');
    }

    public function Download() {

        return $this->hasOne('App\ProductDownload');
    }

}
