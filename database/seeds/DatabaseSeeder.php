<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        DB::table('users')->insert([
            'name' => 'Giulio',
            'email' => 'gdissegna@gmail.com',
            'password' => bcrypt('roger52'),
        ]);

		// $this->call('UserTableSeeder');
	}

}
