@extends('master')

@section('meta')
    @parent
@endsection

@section('content')
    <div id="layerslider">
        <!--layer slider starts-->
        <div class="slider-shadow-top">
        </div>
        <div class="slider-shadow-bottom">
        </div>
        @foreach($sliders as $slider)
            <div class="ls-layer" style="slidedirection: top; slidedelay: 6000; durationin: 1500; durationout: 1500; delayout: 500;">
                <img src="{{ asset($slider->image) }}" class="ls-bg" alt="{{ $slider->alttext }}">
            </div>
        @endforeach
    </div>
    <div id="breadcrumb"><!-- breadcrumb starts-->
        <div class="container">
            <div class="one-half">
                <h4>Prodotti</h4>
            </div>
            <div class="one-half">
                <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                    <ul>
                        <li>Sei qui:</li>
                        <li><a href="/">Home</a></li>
                        <li><a href="/prodotti">Catalogo prodotti</a></li>
                    </ul>
                </nav>
                <!--breadcrumb nav ends -->
            </div>
        </div>
    </div>

    <div id="content">
        <div class="container">
            <div class="one-sixth sidebar left">
                @foreach($categories as $category)
                <div class="widget">
                    <h4 class="widget-title">{{ $category->name }}</h4>
                    <ul class="sidebar-nav">
                        @foreach($category->children as $children)
                        <li><a href="{{ route('ListaProdotti', ['slug' => $children->slug ]) }}" title="Porte in legno">{{ $children->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                @endforeach

            </div>



            <div class="three-fourth">
                <h1>{{ $product->name }}</h1>
                <section class="flex-container">
                    <div class="flexslider single-portfolio-item-slider bottom-margin">
                        <ul class="slides round">
                            @foreach($product->images as $image)
                            <li><img src="{{ asset($image->image) }}" alt=" "/><a href="{{ asset($image->image) }}" class="product-item-preview" data-rel="prettyPhoto"><i class="icon-zoom-in"></i></a></li>
                            @endforeach
                        </ul>
                    </div>

                </section>


                <p>
                    {{ $product->description }}
                </p>
                <p>
                    @if(isset($product->linkfornitore))
                        <a href="{!! $product->linkfornitore !!}" target="_blank">Apri il sito del Produttore</a>
                    @endif
                </p>
                <br>
                <br>

                @if(isset($product->download))

                <a href="{{route('downloads',['slug'=> $product->slug ]) }}" class="button color-alt huge right one-fifth">Download PDF</a>
                @endif

                <a id="cart-prodotto" href="{{ route('cartadditem', ['id' => $product->id ]) }}" value="{{ $product->id }}" v-on="click: AddCart" class="button color-alt huge right one-fifth">Aggiungi al Carrello</a>

            </div>
        </div>
    </div>
    <div class="intro-features"><!-- intro features panel starts -->
        <div class="container">
            <h4>PRODOTTI IN EVIDENZA</h4>
            <div class="slidewrap">
                <!--project carousel starts-->
                <ul class="slider" id="sliderName">
                    <li class="slide"><!-- carousel item starts -->
                        @foreach($homeproducts as $homeproduct)
                        <div class="one-fourth">
                            <div class="item-wrapp">
                                <div class="portfolio-item">
                                    <a href="{{ route('Prodotto',['slug' => $homeproduct->slug ]) }}" class="item-permalink"><i class="icon-link"></i></a>
                                    <a href="{{asset($homeproduct->images[0]->image) }}" data-rel="prettyPhoto" class="item-preview"><i class="icon-zoom-in"></i></a>
                                    <img src="{{ asset($homeproduct->images[0]->image) }}" alt=""/>
                                </div>
                                <div class="portfolio-item-title">
                                    <a href="{{ route('Prodotto',['slug' => $homeproduct->slug ]) }}">{{ $homeproduct->name }}</a>
                                    <p>
                                        {{ str_limit($homeproduct->description, $limit = 30, $end = '...' ) }}
                                        <br>

                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </li>

                </ul><!-- carousel items UL ends -->
            </div>
        </div>
    </div><!-- intro features panel ends -->
    </div>


@endsection