@extends('admin.master')

@section('content')
    <h1>Gestione Categorie</h1>
    <p>Aggiungi, modifica e cancella Categorie</p>

    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right" id="templatemo_sort_btn">
                <a href="{{ route('CategoryCreate') }}" type="button"  class="btn btn-default">Aggiungi nuova Categoria</a>
            </div>
            <div class="table-responsive">
                <h4 class="margin-bottom-15">Tabella Categorie</h4>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>parent</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td class="col-md-2">{!! Html::image($category->image ,'', array('class' => 'img-responsive', 'width' => '300', 'height' => '100' )) !!}</td>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->parent }}</td>
                            <td><a href="{{ route('CategoryEdit', ['id' => $category->id ]) }}" class="btn btn-default">Edit</a></td>
                            <td><a href="{{ route('CategoryDelete', ['id' => $category->id]) }}" class="btn btn-default">Delete</a></td>
                        </tr>
                        @foreach($category->children as $child)
                            <tr>
                                <td>{{ $child->id }}</td>
                                <td class="col-md-2">{!! Html::image($child->image ,'', array('class' => 'img-responsive', 'width' => '300' )) !!}</td>
                                <td>{{ $child->name }}</td>
                                <td>{{ $child->parent }}</td>
                                <td><a href="{{ route('CategoryEdit', ['id' => $child->id]) }}" class="btn btn-default">Edit</a></td>
                                <td><a href="{{ route('CategoryDelete', ['id' => $child->id]) }}" class="btn btn-default">Delete</a></td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>

@endsection