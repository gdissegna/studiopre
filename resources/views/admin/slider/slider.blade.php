@extends('admin.master')


@section('content')
<h1>Manage Slider</h1>
<p>Add change and Delete Images in the slider</p>

<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right" id="templatemo_sort_btn">
            <a href="{{ route('SliderCreate') }}" type="button"  class="btn btn-default">Add New Image</a>
        </div>
        <div class="table-responsive">
            <h4 class="margin-bottom-15">Tabella immagini Slider</h4>
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Image</th>
                    <th>alt text</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sliders as $slider)
                <tr>
                    <td>{{ $slider->id }}</td>
                    <td class="col-md-2">{!! Html::image($slider->image ,'', array('class' => 'img-responsive', 'width' => '300' )) !!}</td>
                    <td>{{ $slider->alttext }}</td>
                    <td><a href="admin/slider/{{ $slider->id }}/edit" class="btn btn-default">Edit</a></td>
                    <td><a href="admin/slider/{{ $slider->id }}/delete" class="btn btn-default">Delete</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
@endsection
