@extends('master')

@section('meta')
    @parent
@endsection

@section('content')

    <div id="content" style="margin-bottom: 25px">
        <div id="breadcrumb"><!-- breadcrumb starts-->
            <div class="container">
                <div class="one-half">
                    <h4>Lo Studio</h4>
                </div>
                <div class="one-half">
                    <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                        <ul>
                            <li>Sei qui:</li>
                            <li><a href="/">Home</a></li>
                            <li>Chi Siamo</li>
                        </ul>
                    </nav><!--breadcrumb nav ends -->
                </div>
            </div>
        </div><!--breadcrumbs ends -->
        <div class="container">
            <div class="one">
                <section class="flex-container">
                    <div class="flexslider single-portfolio-item-slider bottom-margin">
                        <ul class="slides round">
                            <li><img src="images/chisiamo/slider/slider-01.jpg" alt=" "/><a href="images/chisiamo/slider/slider-01.jpg" class="product-item-preview" data-rel="prettyPhoto"><i class="icon-zoom-in"></i></a></li>
                        </ul>
                    </div>

                </section>
            </div>
            <div class="one-third">
                <h4>I nostri Servizi</h4>
                <p>Il nostro Staff è in grado di fornire le migliori soluzioni in ambito di opere e ristrutturazioni edili, progettazione di interni ed esterni, fino alla posa in opera . I nostri tecnici lavorano anche nell'ambito degli impianti rinnovabili.</p>
                <ul class="simple-list">
                    <li><i class="icon-check-sign"></i>Sopralluogo</li>
                    <li><i class="icon-check-sign"></i>Progettazione</li>
                    <li><i class="icon-check-sign"></i>Esecuzione lavori</li>
                    <li><i class="icon-check-sign"></i>Pratiche amministrative</li>
                </ul>
            </div>
            <div class="two-third">
                <h4>Chi Siamo - STUDIO P.R.E.</h4>
                <p>
                    Lo STUDIO P.R.E. si occupa di Progettazioni e Rappresentanze Edili riguardanti nuove costruzioni o ristrutturazioni, nonchè fornitura e posa in opera di serramenti interni ed esterni in varie tipologie e materiali, standard e su misura.
                </p>
                <p>
                    Lo STUDIO P.R.E. vanta un'esperienza trentennale ed ha contribuito, da solo o in collaborazione con altre aziende e professionisti, alla realizzazione di numerose opere edili in regione, in varie parti d'Italia e all'estero - Austria e Croazia - distinguendosi per la precisione e per la serietà del lavoro fornito, sia di tipo progettuale che di assistenza in cantiere, nonchè di fornitura e posa in opera dei manufatti.
                </p>
            </div>
        </div>

    </div>
<!-- intro features panel ends -->
			<div class=" clients-wrapp">
				<div class="container">
					<div class="flexslider clients-slider "><!-- flex slider starts -->
						<ul class="slides client-block">
							<li>
							<div class="one-fifth">
								<img src="images/fornitori/logo-cerrato.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-cocif.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-doors-project.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-ninz.jpg" alt=""/>
							</div>
							<div class="one-fifth ">
								<img src="images/fornitori/logo-steel-project.jpg" alt=""/>
							</div>
							</li>
                            <li>
							<div class="one-fifth">
								<img src="images/fornitori/logo-cerrato.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-cocif.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-doors-project.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-ninz.jpg" alt=""/>
							</div>
							<div class="one-fifth ">
								<img src="images/fornitori/logo-steel-project.jpg" alt=""/>
							</div>
							</li>
						</ul>
					</div><!-- flex slider ends -->
				</div>
			</div>
		</div>
		<section class="footer-call-to-action"><!-- footer call to action starts -->
		<section class="container">
		<div class="three-fourth">
			<h4>I migliori prezzi, i materiali più innovativi, le soluzioni più adatte: Studio PRE ti aspetta !<p>&nbsp;</p></h4>
		</div>
		</section>
		</section><!-- footer call to action ends -->


@endsection