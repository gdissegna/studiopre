@extends('admin.master')

@section('content')
    <h1>Gestione Prodotti</h1>
    <p>Aggiungi, modifica e cancella Prodotti</p>

    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right" id="templatemo_sort_btn">
                <a href="{{ route('ProductCreate') }}" type="button"  class="btn btn-default">Aggiungi nuovo Prodotto</a>
            </div>
            <div class="table-responsive">
                <h4 class="margin-bottom-15">Tabella Prodotti</h4>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Image</th>
                        <th>title</th>
                        <th>Edit Images</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            @if($product->images->count() > 0)
                                <td class="col-md-2">{!! Html::image($product->images[0]->image ,'', array('class' => 'img-responsive', 'width' => '300' )) !!}</td>

                            @else
                                <td class="col-md-2"></td>
                            @endif
                            <td>{{ $product->name }}</td>
                            <td><a href="{{ route('productimages',['slug' => $product->slug]) }}" class="btn btn-default">Edit</a></td>
                            <td><a href="/admin/prodotti/{{ $product->id }}/edit" class="btn btn-default">Edit</a></td>
                            <td><a href="/admin/prodotti/{{ $product->id }}/delete" class="btn btn-default">Delete</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

@endsection