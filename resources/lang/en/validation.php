<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => ":attribute deve essere accettato.",
	"active_url"           => ":attribute non è un indirizzo valido.",
	"after"                => ":attribute deve essere una data successiva a :date.",
	"alpha"                => ":attribute deve contenere solo lettere.",
	"alpha_dash"           => ":attribute deve contenere solo lettere, numeri, e caratteri speciali.",
	"alpha_num"            => ":attribute deve contenere solo letterr e numeri.",
	"array"                => ":attribute deve essere un array.",
	"before"               => ":attribute deve essere una data antecedente :date.",
	"between"              => [
		"numeric" => ":attribute deve essere tra :min e :max.",
		"file"    => ":attribute deve essere tra :min e :max kilobytes.",
		"string"  => ":attribute deve essere tra :min e :max caratteri.",
		"array"   => ":attribute deve essere tra :min e :max elementi.",
	],
	"boolean"              => "Il campo :attribute deve essere vero o falso.",
	"confirmed"            => ":attribute conferma non combacia.",
	"date"                 => ":attribute non è una data valida.",
	"date_format"          => ":attribute non rispecchia il :format.",
	"different"            => ":attribute e :other devono essere diversi.",
	"digits"               => ":attribute deve essere :digits caratteri.",
	"digits_between"       => ":attribute deve essere tra :min e :max caratteri.",
	"email"                => "Il campo :attribute deve essere un indirizzo email valido.",
	"filled"               => "Il campo :attribute è obbligatorio.",
	"exists"               => "Il campo selezionato :attribute non è valido.",
	"image"                => "Il campo :attribute deve essere un'immagine.",
	"in"                   => "Il campo selezionato :attribute non è valido.",
	"integer"              => ":attribute deve essere un integer.",
	"ip"                   => ":attribute deve essere un indirizzo IP valido.",
	"max"                  => [
		"numeric" => ":attribute non deve essere più grande di :max.",
		"file"    => ":attribute non deve essere più grande di :max kilobytes.",
		"string"  => ":attribute non deve essere più grande di :max characters.",
		"array"   => ":attribute non deve avere più di :max items.",
	],
	"mimes"                => ":attribute deve essere un file di tipo: :values.",
	"min"                  => [
		"numeric" => ":attribute deve essere almeno di :min.",
		"file"    => ":attribute deve essere almeno di :min kilobytes.",
		"string"  => ":attribute deve essere almeno di :min caratteri.",
		"array"   => ":attribute deve essere almeno di :min elementi.",
	],
	"not_in"               => ":attribute non è valido.",
	"numeric"              => ":attribute deve essere un numero.",
	"regex"                => "Il formato di :attribute non è valido.",
	"required"             => "Il campo :attribute è richiesto.",
	"required_if"          => "Il campo :attribute è richiesto quando :other è :value.",
	"required_with"        => "Il campo :attribute è richiesto quando :values è presente.",
	"required_with_all"    => "Il campo :attribute è richiesto quando :values è presente.",
	"required_without"     => "Il campo :attribute è richiesto quando :values non è presente.",
	"required_without_all" => "Il campo :attribute è richiesto quando nessuno di :values sono presenti.",
	"same"                 => ":attribute e :other devono combaciare.",
	"size"                 => [
		"numeric" => ":attribute deve essere :size.",
		"file"    => ":attribute deve essere :size kilobytes.",
		"string"  => ":attribute deve essere :size caratteri.",
		"array"   => ":attribute deve contenere :size elementi.",
	],
	"unique"               => ":attribute è stato già preso.",
	"url"                  => "Il formato :attribute non è valido.",
	"timezone"             => ":attribute deve essere una zona valida.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [],

];
