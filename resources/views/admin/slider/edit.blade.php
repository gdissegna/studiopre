@extends('admin.master')


@section('content')
    <a href="/admin" class="btn btn-default">Torna indietro</a>
    <h1>Aggiungi Immagine</h1>
    <p>Aggiungi una Immagine alla slider Formato: jpg,png</p>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['url' => 'admin/slider/{{ $slider->id }}/create','role' => 'form']) !!}
            <div class="form-group">
                <label>Carica L'immagine</label>
                {!! Form::file('filename','$slider->filename', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Inserisci L'alt Text</label>
                {!! Form::text('alt', '$slider->alttext', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection