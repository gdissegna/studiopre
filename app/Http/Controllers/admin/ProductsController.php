<?php namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProductEditRequest;
use App\Product;
use App\ProductDownload;
use App\ProductImage;
use Illuminate\Http\Request;
use App\Http\Requests\ProductCreateRequest;

class ProductsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return view()
     */
    public function index()
    {
        $products = Product::with('images')->get();

        return view('admin.products.products', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View()
     */
    public function create()
    {
        $categories = Category::with('children')->get();

        return view('admin.products.create')->with('categories', $categories);
    }

    /**
     * Store A new Product with images and Pdf
     * @attr ProductCreateRequest $request
     * @return View()
     */
    public function store(ProductCreateRequest $request)
    {
        $product = $request->all();

        $newproduct = New Product();

        $newproduct->name = $product['titolo'];
        $newproduct->description = $product['body'];
        if($request->has('link-fornitore')) {
            $newproduct->linkfornitore = $product['link-fornitore'];
        }

        $newproduct->category_id = $product['categoria'];

        if ($request->has('ishome')) {
            $newproduct->inHomepage = 1;
        } else {
            $newproduct->inHomepage = 0;
        }

        $newproduct->save();


        if (array_key_exists('pdf', $newproduct)) {
            $pdf = new ProductDownload();
            $fileholder = $product['pdf'];
            $filename = date('Y-m-d-h-i-s') . $fileholder->getClientOriginalName();
            $path = public_path('downloads/') . $filename;
            $showpath = 'downloads/' . $filename;
            $fileholder->move(public_path('downloads/'), $filename);
            $pdf->filename = $filename;
            $pdf->file = $showpath;

            $newproduct->Download()->save($pdf);
        }

        \Session::flash('message', 'Prodotto Salvato!');

        return redirect('admin/prodotti/create')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::with('images', 'download')->where('id', $id)->first();

        $categories = Category::with('children')->get();

        return view('admin.products.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param ArticleEditRequest $request
     * @return Response
     */
    public function update($id, ProductEditRequest $request)
    {
        $product = $request->all();

        $newproduct = Product::findOrFail($id);

        $newproduct->name = $product['titolo'];
        $newproduct->description = $product['body'];

        $cat = Category::find($product['categoria']);

        if($request->has('link-fornitore')) {
            $newproduct->linkfornitore = $product['link-fornitore'];
        }


        if (array_key_exists('ishome', $product)) {
            $newproduct->inHomepage = true;
        } else {
            $newproduct->inHomepage = false;
        }

        // $newproduct->save();

        $newproduct = $cat->products()->save($newproduct);


        if (array_key_exists('pdf', $product)) {

            $pdf = ProductDownload::where('product_id', $newproduct->id)->first();

            if (!isset($pdf)) {

                $pdf = new ProductDownload();

                $fileholder = $product['pdf'];
                $filename = date('Y-m-d-h-i-s') . $fileholder->getClientOriginalName();
                $path = public_path('downloads/') . $filename;
                $showpath = 'downloads/' . $filename;
                $fileholder->move(public_path('downloads/'), $filename);
                $pdf->filename = $filename;
                $pdf->file = $showpath;

                $newproduct->Download()->save($pdf);

            } else {

                $fileholder = $product['pdf'];
                $filename = date('Y-m-d-h-i-s') . $fileholder->getClientOriginalName();
                $path = public_path('downloads/') . $filename;
                $showpath = 'downloads/' . $filename;
                $fileholder->move(public_path('downloads/'), $filename);
                $pdf->filename = $filename;
                $pdf->file = $showpath;
                $pdf->save();
            }

        }

        \Session::flash('message', 'Prodotto Salvato!');

        return redirect()->route('ProductEdit', [$newproduct->id])->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::with('images', 'download')->where('id', $id)->first();

        foreach ($product->Images as $image) {
            if (\File::isFile($image->image)) {
                \File::delete($image->image);
            }
        }


        if ($product->download != Null) {
            if (\File::isFile($product->Download->file)) {
                \File::delete($product->Download->file);
            }
        }

        $product->delete();

        return redirect('admin/prodotti');
    }

}
