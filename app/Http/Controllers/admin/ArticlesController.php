<?php namespace App\Http\Controllers\admin;

use App\Article;
use App\ArticleImage;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ArticleCreateRequest;
use App\Http\Requests\ArticleEditRequest;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ArticlesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$articles = Article::with('images')->get();

        return view('admin.articles.articles', compact('articles'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.articles.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ArticleCreateRequest $request)
	{
		$postarticle = $request->all();

        $article = new Article();

        $article->title = $postarticle['titolo'];
        $article->body = $postarticle['body'];
        $article->save();

        $articleimages = $postarticle['images'];

            foreach ($articleimages as $image) {

                $newImage = new ArticleImage();

                $filename = $image->getClientOriginalName();
                $path = public_path('images/articlesImages/') . date('Y-m-d-h-i-s') . $filename;
                $showPath = 'images/articlesImages/' . date('Y-m-d-h-i-s') . $filename;
                Image::make($image)->resize(1920, null)->resizeCanvas(null, 550)->save($path);
                $newImage->filename = $filename;
                $newImage->image = $showPath;

                $article->images()->save($newImage);

            }


        \Session::flash('message', 'Articolo Salvato!');

        return redirect('admin/articoli/create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $article = Article::findOrFail($id);

        return view('admin.articles.edit', compact('article'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, ArticleEditRequest $request)
	{
        $postarticle = $request->all();


        $article = Article::find($id);

        $article->title = $postarticle['titolo'];
        $article->body = $postarticle['body'];
        $article->save();

        $articleimages = $postarticle['images'];

        foreach($articleimages as $image ) {

            $filename = $image->getClientOriginalName();
            $path = public_path('images/articlesImages/') . date('Y-m-d-h-i-s') . $filename;
            $showPath = 'images/articlesImages/' . date('Y-m-d-h-i-s') . $filename;
            Image::make($image)->resize(1920, null)->resizeCanvas(null, 550)->save($path);
            $newImage->filename = $filename;
            $newImage->image = $showPath;

            $article->images()->save($newImage);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

        $article = Article::find($id)->Images;

        foreach($article as $image) {

            if (\File::isFile($image->image)) {
                \File::delete($image->image);
            }
        }

        Article::find($id)->delete();

        return \Redirect::to('admin/articoli');
	}

}
