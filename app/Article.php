<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;


class Article extends Model implements SluggableInterface {

    use SluggableTrait;

    protected $table = 'articles';
    protected $fillable = [];

    protected $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Images() {

        return $this->hasMany('App\ArticleImage', 'article_id');
    }

}