<link rel="stylesheet" href="{{ asset('css/style.css') }}" media="screen"/><!-- MAIN STYLE CSS FILE -->
<link rel="stylesheet" href="{{ asset('css/responsive.css') }}" media="screen"/><!-- RESPONSIVE CSS FILE -->
<link rel="stylesheet" id="style-color" href="{{ asset('css/colors/blue-color.css') }}" media="screen"/><!-- DEFAULT BLUE COLOR CSS FILE -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700,500' rel='stylesheet' type='text/css'><!-- ROBOTO FONT FROM GOOGLE CSS FILE -->
<link rel="stylesheet" href="{{ asset('css/prettyPhoto.css') }}" media="screen"/><!--PRETTYPHOTO CSS FILE -->
<link rel="stylesheet" href="{{ asset('css/font-awesome/font-awesome.min.css') }}" media="screen"/><!-- FONT AWESOME ICONS CSS FILE -->
<link rel="stylesheet" href="{{ asset('css/layer-slider.css') }}" media="screen"/><!-- LAYER SLIDER CSS FILE -->
<link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" media="screen"/><!-- FLEX SLIDER CSS FILE -->
<link rel="stylesheet" href="{{ asset('css/revolution-slider.css') }}" media="screen"/><!-- REVOLUTION SLIDER CSS FILE -->




