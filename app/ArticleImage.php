<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleImage extends Model {

    protected $table = "article_images";

    protected $fillable = [];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Article() {

        return $this->belongsTo('App\Article');
    }


}
