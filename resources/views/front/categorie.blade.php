@extends('master')

@section('title')
@endsection

@section('meta')
    @parent
@endsection

@section('content')
    <div id="layerslider">
        <!--layer slider starts-->
        <div class="slider-shadow-top">
        </div>
        <div class="slider-shadow-bottom">
        </div>
        @foreach($sliders as $slider)
            <div class="ls-layer" style="slidedirection: top; slidedelay: 6000; durationin: 1500; durationout: 1500; delayout: 500;">
                <img src="{{ $slider->image }}" class="ls-bg" alt="{{ $slider->alttext }}">
            </div>
        @endforeach
    </div>
    <div id="breadcrumb"><!-- breadcrumb starts-->
        <div class="container">
            <div class="one-half">
                <h4>prodotti</h4>
            </div>
            <div class="one-half">
                <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                    <ul>
                        <li>You are here:</li>
                        <li><a href="/">Home</a></li>
                        <li><a href="/prodotti">prodotti</a></li>
                    </ul>
                </nav>
                <!--breadcrumb nav ends -->
            </div>
        </div>
    </div>
    <div id="content" style="margin-bottom: 15px;">
        <div class="container">
            <div class="four-columns">
                @foreach($categories as $category)
                    <div class="category_top">{{ $category->name }}</div>
                    <div class="container clearfix">
                    @foreach($category->children as $children)

                        <div class="one-fourth">
                            <div class="item-wrapp">
                                <div class="portfolio-item">
                                    <a href="{{ route('ListaProdotti', ['slug' => $children->slug ]) }}" class="item-permalink"><i class="icon-link"></i></a>
                                    <a href="{{ $children->image }}" data-rel="prettyPhoto" class="item-preview"><i class="icon-zoom-in"></i></a>
                                    <img src="{{ $children->image }}" alt=""/>
                                </div>
                                <div class="portfolio-item-title">
                                    <a href="{{ route('ListaProdotti', ['slug' => $children->slug ]) }}">{{ $children->name }}</a>
                                    <p>
                                        {{ str_limit($children->description, $limit = 30, $end = '...' ) }}
                                         <br>
                                       Numero prodotti: {{ $children->Products->count() }}
                                    </p>
                                </div>
                            </div>
                        </div>

                    @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container clearfix" style="height: 20px;"></div>
    <section class="footer-call-to-action"><!-- footer call to action starts -->
        <section class="container">
            <div class="three-fourth">
                <h4>I migliori prezzi, i materiali più innovativi, le soluzioni più adatte: Studio PRE ti aspetta !<p>&nbsp;</p></h4>
            </div>
        </section>
    </section>
@endsection