<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDownload extends Model {

    protected $table = "product_downloads";

    protected $fillable = [];
    protected $guarded = [];

    public function Product() {
        return $this->belongsTo('App\Product');
    }

}
