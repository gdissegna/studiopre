<!DOCTYPE html>
<html lang="it" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>STUDIO PRE - Studio progettazione e vendita serramenti porte portoni @yield('title')</title>
    <link rel="shortcut icon" href="images/favicon.gif"/>
    <meta name="_token" content="{{ csrf_token() }}" />

@section('meta')
    @show
    @include('front.partials.css')
</head>

<body>
<div id="container">
    <!-- main container starts-->
    <div id="wrapp">
        <!-- main wrapp starts-->
        @section('header')
            <header id="header">
                <!--header starts -->
                <div id="header-links">
                    <div class="container">
                        <div class="one-half">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="head-wrapp">
                        <div class="one-half">
                        <a title="test tooltip" data-html="true" data-placement="bottom" href="index.html" id="logo"><img src=" {{ asset('images/logo.png') }}" alt=""/>
                        </a>
                        </div>
                        <!--your logo-->

                    </div>
                </div>
                <div id="main-navigation">
                    <div class="container">
                        <ul class="main-menu">
                            <li><a href="/">Home</a></li>
                            <li><a href="/lo-studio">Lo Studio</a></li>
                            <li><a href="/prodotti">Catalogo Prodotti</a></li>
                            <li><a href="/servizi-chiavi-in-mano">Servizi Chiavi in Mano</a></li>
                            <li><a href="/contatti">Contatti</a></li>
                            <li><a href="/carrello">Carrello preventivi</a></li>
                            
                        </ul>
                    </div>
                </div>
            </header>
        @show
    <!-- Main Content Section -->
    @yield('content')
    <!-- Main Content Section END -->
    <!-- Footer Section -->
    @section('footer')
        <section id="copyrights">
            <section class="container">
                <div class="one-half">
                    <p>
                        Copyright @2016 <a href="http://www.studiopre.com"
                                                  target="_blank">STUDIO PRE S.r.l. P. IVA IT00957280308</a>
                    </p>
                </div>
                <div class="one-half">
                    <ul class="copyright_links">
                        <li><a href="/">Home</a></li>
                        <li><a href="/lo-studio">Lo Studio</a></li>
                        <li><a href="/prodotti">Catalogo Prodotti</a></li>
                        <li><a href="/contatti">Contatti</a></li>
                        <li><a href="/servizi-chiavi-in-mano">Servizi Chiavi in Mano</a></li>
                        <li><a href="/note-legali">Note Legali</a></li>
                        <li><a href="/privacy">Privacy</a></li>
                    </ul>
                </div>
            </section>
        </section>
    @show
                <!-- Footer Section END -->
    </div>
</div>
@include('front.partials.js')
@include('front.partials.analytics')
</body>
</html>
