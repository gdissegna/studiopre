@extends('admin.master')


@section('content')
    <a href="/admin" class="btn btn-default">Torna indietro</a>
    <h1>Aggiungi Immagine</h1>
    <p>Aggiungi una Immagine alla slider Formato: jpg,png</p>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['url' => 'admin/slider/create','role' => 'form','files'=>true ]) !!}
                <div class="form-group">
                    <label>Carica L'immagine</label>
                    {!! Form::file('filename', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>Inserisci L'alt Text</label>
                    {!! Form::text('alt', 'Alt', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection

