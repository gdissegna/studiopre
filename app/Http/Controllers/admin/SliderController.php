<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\SliderCreateRequest;
use App\Http\Requests\SliderEditRequest;
use App\SliderImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{

    protected $slider;


    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function index()
    {
        $sliders = SliderImage::all();

        return view('admin.slider.slider', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param SliderCreateRequest $request
     * @var $newSlider
     * @return Response
     */
    public function store(SliderCreateRequest $request)
    {


        $newSlider = $request->all();

        $slider = new SliderImage();

        $slider->filename = $newSlider['filename']->getClientOriginalName();

        //$path = 'D:\lamp\www\studiopre\public/images\sliders\\' . date('Y-m-d-h-i-s') . $slider->filename;

        $path = public_path('images/sliders/') . date('Y-m-d-h-i-s') . $slider->filename;
        $showpath = 'images/sliders/' . date('Y-m-d-h-i-s') . $slider->filename;
        Image::make($newSlider['filename'])->resize(1920, null)->resizeCanvas(null, 550)->save($path);
        $slider->image = $showpath;
        $slider->mime = $newSlider['filename']->getClientMimeType();
        $slider->alttext = $newSlider['alt'];
        $slider->save();

        return Redirect::to('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @param  SliderEditRequest $request
     * @return Response
     */
    public function edit($id, SliderEditRequest $request)
    {
        $slider = SliderImage::findOrFail($id);

        return view('admin.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $slider = SliderImage::find($id);
        if(\File::isFile($slider->image)) {
            \File::delete($slider->image);
        }
        $slider->delete();
        return Redirect::to('admin');
    }
}
