<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script><!-- JQUERY JS FILE -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><!-- GOOGLE MAP SENSOR JS FILE -->
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script><!-- JQUERY UI JS FILE -->
<script type="text/javascript" src="{{ asset('js/flex-slider.min.j') }}s"></script><!-- FLEX SLIDER JS FILE -->
<script type="text/javascript" src="{{ asset('js/navigation.min.js') }}"></script><!-- MAIN NAVIGATION JS FILE -->
<script type="text/javascript" src="{{ asset('js/jquery.layerslider.js') }}"></script><!-- LAYER SLIDER JS FILE -->
<script type="text/javascript" src="{{ asset('js/layerslider.transitions.js') }}"></script><!-- LAYER SLIDER TRANSITIONS JS FILE -->
<script type="text/javascript" src="{{ asset('js/map.min.js') }}"></script><!-- GOOGLE MAP JS FILE -->
<script type="text/javascript" src="{{ asset('js/carousel.js') }}"></script><!-- CAROUSEL SLIDER JS -->
<script type="text/javascript" src="{{ asset('js/jquery.theme.plugins.min.js') }}"></script><!-- REVOLUTION SLIDER PLUGINS JS FILE -->
<script type="text/javascript" src="{{ asset('js/jquery.themepunch.revolution.min.js') }}"></script><!-- REVOLUTION SLIDER JS FILE -->
<script type="text/javascript" src="{{ asset('js/flickr.js')}}"></script><!-- FLICKR WIDGET JS FILE -->
<script type="text/javascript" src="{{ asset('js/instagram.js')}}"></script><!-- INSTAGRAM WIDGET JS FILE -->
<script type="text/javascript" src="{{ asset('js/prettyPhoto.min.js') }}"></script><!-- PRETTYPHOTO JS FILE -->
<script type="text/javascript" src="{{ asset('js/jquery.tooltips.min.js') }}"></script><!-- TOOLTIPS JS FILE -->
<script type="text/javascript" src="{{ asset('js/isotope.min.js') }}"></script><!--ISOTOPE JS FILE -->
<script type="text/javascript" src="{{ asset('js/scrolltopcontrol.js') }}"></script><!-- SCROLL TO TOP JS PLUGIN -->
<script type="text/javascript" src="{{ asset('js/jquery.easy-pie-chart.js') }}"></script><!-- EASY PIE JS FILE -->
<script type="text/javascript" src="{{ asset('js/jquery.transit.min.js') }}"></script><!-- TRANSITION JS FILE -->
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script><!-- TRANSITION JS FILE -->
<script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script><!-- CUSTOM JAVASCRIPT JS FILE -->
<script type="text/javascript" src="{{ asset('js/cart.js') }}"></script><!-- CUSTOM JAVASCRIPT JS FILE -->
<script type="text/javascript" src="{{ asset('js/analytics.js') }}"></script><!-- CUSTOM JAVASCRIPT JS FILE -->



