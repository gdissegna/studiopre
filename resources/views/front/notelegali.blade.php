@extends('master')

@section('meta')
    @parent
@endsection

@section('content')

    <div id="content" style="margin-bottom: 25px">
        <div id="breadcrumb"><!-- breadcrumb starts-->
            <div class="container">
                <div class="one-half">
                    <h4>Single Project 1</h4>
                </div>
                <div class="one-half">
                    <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                        <ul>
                            <li>Sei qui:</li>
                            <li><a href="/">Home</a></li>
                            <li>Note legali</li>
                        </ul>
                    </nav><!--breadcrumb nav ends -->
                </div>
            </div>
        </div><!--breadcrumbs ends -->
        <div class="container">
            <div class="one">
                <section class="flex-container">
                    <div class="flexslider single-portfolio-item-slider bottom-margin">
                        <ul class="slides round">
                            <li><img src="images/legal/legal-placeholder.jpg" alt=" "/></li>
                        </ul>
                    </div>

                </section>
            </div>
            <div class="one">
                <h4>Avvertenze sulle informazioni relative a prodotti e servizi</h4>
                <p>
                    Le informazioni presenti sul sito www.studiopre.com hanno natura di messaggio pubblicitario con finalità promozionale. I colori, le immagini, le descrizioni, le misure dei prodotti presentati sono esclusivamente illustrativi e non costituiscono attestazione o garanzia delle effettive caratteristiche dei modelli in vendita.
                </p>
                <p>
                    Copyright: I marchi, i modelli, i brevetti, i testi, le immagini, il materiale visuale, il software, presenti nel sito, sono di proprietà di STUDIO PRE S.r.l. o comunque da questa usati col consenso dei titolari.
                </p>
                <h4>RECEPIMENTO ART.42, L. 7.7.2009, N. 88 (COMUNITARIA 2008)</h4>
                <p>
                  In base ai nuovi obblighi di pubblicità sul web introdotti dall'articolo di legge di cui sopra, rendiamo noti i seguenti dati societari:
                </p>
                <p>
                    STUDIO PRE S.r.l.<br>
Piazza Mazzini, 10<br>
33019 Tricesimo (UDINE) - Italy<br>
                    E-mail: <a href="mailto:info@studiopre.com">info@studiopre.com</a> - Web: <a href="http://www.studiopre.com" target="_blank">http://www.studiopre.com</a><br>
Reg.impr. C.F. P.IVA IT00957280308<br>
Numero d’identificazione CEE IT00957280308<br>
Cod. R.E.A UD247791<br>
Capitale sociale € 150.000,00 i.v.<br>
Tel. ++39.0432.851776<br>
Fax. ++39.0432.851777<br>
                </p>

            </div>
        </div>

    </div>


@endsection