@extends('admin.master')


@section('content')
    <a href="/admin/prodotti" class="btn btn-default">Torna indietro</a>
    <h1>Edita il Prodotto</h1>
    <p>Edita i dati del Prodotto</p>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['route' => ['ProductEditPost', $product->id ],'role' => 'form', 'files'=>true ]) !!}
            <div class="form-group">
                <label>Nome Prodotto</label>
                {!! Form::text('titolo', $product->name , ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Descrizione Prodotto</label>
                {!! Form::textarea('body', $product->description, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Aggiungi link fornitore</label>

                {!! Form::text('link-fornitore', $product->linkfornitore, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Edita scheda Pdf</label>
                <p>{!! ($product->download === null ? ' ' : $product->download->file ) !!}</p>
                {!! Form::file('pdf', '') !!}
            </div>
            <label>Seleziona Categoria Parente</label>
            <select name="categoria" class="form-control">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" {{ ($product->category_id === $category->id ? ' selected="selected"' : '') }}>{{ $category->name }}</option>
                @endforeach
            </select>
            <div class="form-group">
                <label>
                    {!! Form::checkbox('ishome', '1', null,  ['id' => 'ishome']) !!}
                    Seleziona se il prodotto deve essere mostrato in Homepage
                </label>
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection