@extends('admin.master')


@section('content')
    <a href="/admin/categorie" class="btn btn-default">Torna indietro</a>
    <h1>Aggiungi una Categoria</h1>
    <p>Crea una nuova Categoria</p>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['url' => 'admin/categorie/create','role' => 'form','files'=>true ]) !!}
            <div class="form-group">
                <label>Nome Categoria</label>
                {!! Form::text('nome', 'nome', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Inserisci la descrizione</label>
                {!! Form::textarea('descrizione', 'descrizione', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Seleziona Categoria Parente</label>
                <select name="parent" class="form-control">
                    <option value="0">nessuna</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ (Input::old('name', 0) === $category->id ? ' selected="selected"' : '') }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Aggiungi l'immagine</label>
                {!! Form::file('image', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection