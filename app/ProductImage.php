<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model {

	protected $table = "product_images";

    protected $fillable = [];

    protected $guarded =[];



    public function Item() {

        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

}
