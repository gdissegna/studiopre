<?php

namespace App\Http\Controllers;

use App\Product;
use App\Article;
use App\SliderImage;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sliders = SliderImage::all();

        $products = Product::with('images')->where('inHomepage', 1)->get();

        // $articles = Article::orderBy('created_at')->get(3);

        return view('front.index')->with('sliders', $sliders)->with('products', $products);
    }

}
