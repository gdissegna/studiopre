@extends('master')

@section('meta')
    @parent
@endsection

@section('content')

    <div id="content" style="margin-bottom: 25px">
        <div id="breadcrumb"><!-- breadcrumb starts-->
            <div class="container">
                <div class="one-half">
                    <h4>Servizi chiavi in mano</h4>
                </div>
                <div class="one-half">
                    <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                        <ul>
                            <li>Sei qui:</li>
                            <li><a href="/">Home</a></li>
                            <li>Servizi chiavi in mano</li>
                        </ul>
                    </nav><!--breadcrumb nav ends -->
                </div>
            </div>
        </div><!--breadcrumbs ends -->
        <div class="container">
            <div class="one">
                <section class="flex-container">
                    <div class="flexslider single-portfolio-item-slider bottom-margin">
                        <ul class="slides round">
                            <li><img src="images/chisiamo/slider/slider-01.jpg" alt=" "/><a href="images/slider/slider-01.jpg" class="product-item-preview" data-rel="prettyPhoto"><i class="icon-zoom-in"></i></a></li>
                        </ul>
                    </div>

                </section>
            </div>
            <div class="one">
                <h4>STUDIO P.R.E.</h4>
                <p>
                    Lo Studio P.R.E. si occupa di Progettazioni e Rappresentanze Edili riguardanti nuove costruzioni o ristrutturazioni, nonchè fornitura e posa in opera di serramenti interni ed esterni in varie tipologie e materiali, standard e su misura.
                </p>
                <p>
                    Lo Studio P.R.E. vanta un'esperienza trentennale ed ha contribuito, da solo o in collaborazione con altre aziende e professionisti, alla realizzazione di numerose opere edili in regione, in varie parti d'Italia e all'estero - Austria e Croazia - distinguendosi per la precisione e per la serietà del lavoro fornito, sia di tipo progettuale che di assistenza in cantiere, nonchè di fornitura e posa in opera dei manufatti.
                </p>
                <p>
                    Contattateci per avere la migliore soluzione alle Vostre esigenze! Chiamateci allo 0432 851776.
                </p>

            </div>
        </div>

    </div>


@endsection