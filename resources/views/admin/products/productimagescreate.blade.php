@extends('admin.master')


@section('content')
    <a href="/admin/categorie" class="btn btn-default">Torna indietro</a>
<h1>Aggiungi nuova Immagine al Prodotto </h1>
<p>Crea Nuova Immagine</p>
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (Session::has('message'))
<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<div class="row">

    <!--   <form>
           <input type="file" name="images[]" multiple>
           <input type="submit" name="Submit">
       </form> -->

    <div class="col-md-6">
        {!! Form::open(['route' => array('productimagescreatepost', $product->slug),'role' => 'form', 'files'=>true ]) !!}
        <div class="form-group">
            <label>Testo attributo Alt</label>
            {!! Form::text('alttext', 'alttext', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label>Aggiungi immagine</label>
            {!! Form::file('image', '') !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}

    </div>
</div>
@endsection