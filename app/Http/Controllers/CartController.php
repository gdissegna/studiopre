<?php

namespace App\Http\Controllers;


use App\Category;
use App\Product;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CartFormRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Jobs\SendCartEmail;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     * @var array $cart
     * @return Response
     */
    public function showcartform()
    {
        $cart = Session::get('cart');

        $cartitems = Product::whereIn('id', $cart)->get();

        $categories = Category::all();

        return view('front.carrello')->with('cartitems', $cartitems);

    }

    public function postcartform(CartFormRequest $request)
    {
        $cartitems = Session::get('cart');

        $cart = Product::whereIn('id',$cartitems)->get();


        $job = (new SendCartEmail($request,$cart));

        $this->dispatch($job);

        Session::forget('cart');

        Session::flash('message', 'Il form e stato correttamente inviato');

        return redirect()->back();
    }


    /**
     * Add a product to the cart
     *
     */
    public function additem($id)
    {

        Session::push('cart', $id);

        return redirect()->back();
    }

    /**
     * remove a product from the cart
     *
     */
    public function removeitem($id)
    {
        Session::pull('cart', $id);

        return redirect()->back()->withInput();
    }

}
