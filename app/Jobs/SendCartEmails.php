<?php

namespace App\Jobs;

use Mail;
use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class SendCartEmail extends Job implements SelfHandling
{


    protected $request;
    protected $cart;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request,$cart)
    {
        $this->request = $request;
        $this->cart = $cart;
    }

    /**
     * Execute the job.
     *
     * @var $request
     * @var $cart
     *
     * @return view
     */
    public function handle()
    {
        Mail::send('emails.cartform', ['request' => $this->request,'cart' => $this->cart], function($message) {
            $message->from('info@studiopre.com', 'studio Pre');
            $message->to($this->request['email'], $this->request['name'] . $this->request['cognome'] );
            $message->subject('STUDIO PRE, Grazie per averci contatto vi rispoderemo al piu presto');
        });

        Mail::send('emails.cartforminternal', ['request' => $this->request,'cart' => $this->cart], function($message) {
            $message->from('info@studiopre.com', 'studio Pre');
            $message->to('info@triangolo.it');
            $message->subject('STUDIO PRE nuovo carrello');
        });

    }
}
