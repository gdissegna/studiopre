<?php namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryEditRequest;


class CategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::with('children')->where('parent', '0')->get();

        return view('admin.categories.categories', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::where('parent', '0')->get();

        return view('admin.categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param CategoryCreateRequest $request
     * @return Response
     */
    public function store(CategoryCreateRequest $request)
    {
        $category = $request->all();

        $newcategory = New Category();

        $newcategory->name = $category['nome'];
        $newcategory->description = $category['descrizione'];

        $imagename = $category['image']->getClientOriginalName();
        $path = public_path('images/categories/') . date('Y-m-d-h-i-s') . $imagename;
        $showpath = 'images/categories/' . date('Y-m-d-h-i-s') . $imagename;
        \Image::make($category['image'])->resize(540, null)->resizeCanvas(null, 372)->save($path);

        $newcategory->image = $showpath;
        $newcategory->parent = $category['parent'];
        $newcategory->save();

        \Session::flash('message', 'Categoria Salvata!');

        return redirect('admin/categorie/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $category_edit = Category::find($id);


        $categories = Category::where('parent', 0)->get();


        return view('admin.categories.edit', compact('categories', 'category_edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param CategoryEditRequest $request
     * @return Response
     */
    public function update($id, CategoryEditRequest $request)
    {
        $category = $request->all();

        $newcategory = Category::find($id);

        $newcategory->name = $category['nome'];
        $newcategory->description = $category['descrizione'];
        if (array_key_exists('image', $category)) {
            $imagename = $category['image']->getClientOriginalName();
            $path = public_path('images/categories/') . date('Y-m-d-h-i-s') . $imagename;
            $showpath = 'images/categories/' . date('Y-m-d-h-i-s') . $imagename;
            \Image::make($category['image'])->resize(540, null)->resizeCanvas(null, 372)->save($path);
            $newcategory->image = $showpath;
        }

        $cat_parent = Category::find($category['parent']);

        if ($category['parent'] == 0) {

            $newcategory->parent = 0;

            $newcategory->save();
        } else {
            $cat_parent->Children()->save($newcategory);
        }


        //$newcategory->parent = $category['parent'];
        //$newcategory->save();

        //return redirect('admin.categories.categories');
        \Session::flash('message', 'Categoria Salvata!');

        return redirect()->route('CategoryEdit', [$newcategory->id])->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if ($category->image != null) {
            if (\File::isFile($category->image)) {
                \File::delete($category->image);
            }
        }

        if ($category->parent == 0) {
            $childrens = Category::where('parent', $category->id)->get();

                foreach ($childrens as $children) {
                    if ($children->image != null) {
                        if (\File::isFile($children->image)) {
                            \File::delete($children->image);
                        }
                    }
                    $children->delete();
                }


        }


        $category->delete();

        return redirect('admin/categorie');
    }

}
