<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StaticPagesController extends Controller
{
    /**
     * Lancia la pagina Chi Siamo
     * @return view
     */
    public function chisiamo()
    {
        return view('front.chisiamo');
    }

    public function notelegali()
    {
        return view('front.notelegali');
    }

    public function privacy()
    {
        return view('front.privacy');
    }

    public function chiaviinmano()
    {
        return view('front.servizi-chiavi');
    }

}
