@extends('admin.master')


@section('content')
    <a href="/admin/prodotti" class="btn btn-default">Torna indietro</a>
    <h1>Aggiungi nuovo Prodotto</h1>
    <p>Crea un nuovo Prodotto</p>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="row">

     <!--   <form>
            <input type="file" name="images[]" multiple>
            <input type="submit" name="Submit">
        </form> -->

        <div class="col-md-6">
            {!! Form::open(['url' => 'admin/prodotti/create','role' => 'form', 'files'=>true ]) !!}
            <div class="form-group">
                <label>Nome Prodotto</label>
                {!! Form::text('titolo', 'titolo', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Descrizione Prodotto</label>
                {!! Form::textarea('body', 'body', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Aggiungi link fornitore</label>
                {!! Form::text('link-fornitore', 'link-fornitore', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Aggiungi scheda Pdf</label>
                {!! Form::file('pdf', '') !!}
            </div>
            <label>Seleziona Categoria Parente</label>
            <select name="categoria" class="form-control">

                @foreach($categories as $category)
                    <option value="{{ $category->id }}" {{ (Input::old('name', 0) === $category->id ? ' selected="selected"' : '') }}>{{ $category->name }}</option>
                @endforeach
            </select>
            <div class="form-group">
                <label>
                    {!! Form::checkbox('ishome', '1', 0,  ['id' => 'ishome']) !!}
                    Seleziona se il prodotto deve essere mostrato in Homepage
                </label>
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection

