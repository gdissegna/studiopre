<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Category extends Model implements SluggableInterface {

	use SluggableTrait;

    protected $table = "categories";

    protected $fillable = ['name','description','parent','image'];
    protected $guarded = ['id'];

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug',
    );


    public function Products() {

        return $this->hasMany('App\Product');
    }

    /**
     * @return Collection
     */
    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent')->where('parent', 0);
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent');
    }

}
