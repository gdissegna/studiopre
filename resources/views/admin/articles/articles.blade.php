@extends('admin.master')

@section('content')
    <h1>Gestione Articoli</h1>
    <p>Aggiungi, modifica e cancella Articoli</p>

    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right" id="templatemo_sort_btn">
                <a href="{{ route('ArticleCreate') }}" type="button"  class="btn btn-default">Agiiungi nuovo Articolo</a>
            </div>
            <div class="table-responsive">
                <h4 class="margin-bottom-15">Tabella Articoli</h4>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Image</th>
                        <th>title</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{ $article->id }}</td>
                            <td class="col-md-2">{!! Html::image($article->images[0]->image ,'', array('class' => 'img-responsive', 'width' => '300' )) !!}</td>
                            <td>{{ $article->title }}</td>
                            <td><a href="/admin/articoli/{{ $article->id }}/edit" class="btn btn-default">Edit</a></td>
                            <td><a href="/admin/articoli/{{ $article->id }}/delete" class="btn btn-default">Delete</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

@endsection