@extends('master')

@section('meta')
    @parent
@endsection

@section('content')

    <div id="content" style="margin-bottom: 25px">
        <div id="breadcrumb"><!-- breadcrumb starts-->
            <div class="container">
                <div class="one-half">
                    <h4>PRIVACY</h4>
                </div>
                <div class="one-half">
                    <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                        <ul>
                            <li>Sei qui:</li>
                            <li><a href="/">Home</a></li>
                            <li>Privacy</li>
                        </ul>
                    </nav><!--breadcrumb nav ends -->
                </div>
            </div>
        </div><!--breadcrumbs ends -->
        <div class="container">
            <div class="one">
                <section class="flex-container">
                    <div class="flexslider single-portfolio-item-slider bottom-margin">
                        <ul class="slides round">
                            <li><img src="images/privacy/privacy-placeholder.jpg" alt=" "/></li>
                        </ul>
                    </div>

                </section>
            </div>
            <div class="one">
                <h4>CODICE IN MATERIA DI PROTEZIONE DEI DATI PERSONALI</h4>
                <p>
                    Lo STUDIO P.R.E. vi dà il benvenuto nel sito web aziendale. Prima di proseguire nella consultazione del sito Vi invita a leggere con attenzione questa pagina.
                </p>
                <p>
                    In riferimento al D.lgs 196/2003 (codice in materia di protezione dei dati personali), Le comunichiamo che il trattamento dei suoi dati personali è finalizzato alla fornitura del servizio in oggetto (navigazione del sito, richiesta informazioni, registrazione per servizi) ed all'invio di nostro materiale promozionale e/o pubblicitario. Le ricordiamo altresì che, per la gestione della sua richiesta, i suoi dati potranno essere comunicati anche a soggetti esterni che svolgono attività connesse e funzionali all'operatività del servizio (come: gestione del sistema informatico e telematico).
                </p>
                <p>
                   Tali funzioni sono fondamentali per l'esecuzione dell'operazione.<br>
                    Il titolare del trattamento dei dati è STUDIO P.R.E. S.r.l.
                </p>
                <p>
                    Lei può esercitare i diritti di cui all'Art.7 del D.lgs 196/2003 rivolgendosi a:
                </p>
                <p>
                    STUDIO P.R.E. S.r.l.<br>
                    Piazza Mazzini n. 10<br>
                    33019 Tricesimo (Udine)<br>
                    Italy<br>
                    <a href="mailto:info@studiopre.com">info@studiopre.com</a>
                </p>

            </div>
        </div>

    </div>


@endsection