@extends('admin.master')


@section('content')
    <h1>Aggiungi un articolo</h1>
    <p>Crea un nuovo Articolo</p>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['url' => 'admin/articoli/create','role' => 'form','files'=>true ]) !!}
            <div class="form-group">
                <label>Titolo Articolo</label>
                {!! Form::text('titolo', 'titolo', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Inserisci L'alt Text</label>
                {!! Form::textarea('body', 'body', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Aggiungi le immagini</label>
                {!! Form::file('images[]', array('multiple'=>true)) !!}
            </div>
            <div class="form-group">
                <label>
                	{!! Form::checkbox('ishome', '1', null,  ['id' => 'ishome']) !!}
                    Seleziona se L'articolo deve essere mostrato in Homepage
                </label>
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection