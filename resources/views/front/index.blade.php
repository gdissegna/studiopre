@extends('master')

@section('title')
@endsection

@section('meta')
    @parent
@endsection

@section('content')
    <div id="layerslider">
        <!--layer slider starts-->
        <div class="slider-shadow-top">
        </div>
        <div class="slider-shadow-bottom">
        </div>
        @foreach($sliders as $slider)
        <div class="ls-layer" style="slidedirection: top; slidedelay: 6000; durationin: 1500; durationout: 1500; delayout: 500;">
            <img src="{{ $slider->image }}" class="ls-bg" alt="{{ $slider->alttext }}">
        </div>
        @endforeach
    </div>
    <div id="content">
    <div class="home-intro"><!-- home intro starts -->
        <div class="container">
            <div class="three-fourth">
                <h4>Dal 1970 progettiamo e realizziamo gli interni dei tuoi sogni.</h4>
                <p>
                    Lo Studio P.R.E. si occupa di Progettazioni e Rappresentanze Edili riguardanti nuove costruzioni o ristrutturazioni.
                </p>
            </div>
            
        </div>
    </div>
    <!--home intro ends-->
    <div class="container">
        <div class="one-third">
            <div class="feature-block"><!-- features blocks starts -->
                <div class="feature-block-title">
                    <div class="feature-block-icon">
                        <i class="icon-lightbulb"></i><span></span>
                    </div>
                    <h4>PROGETTAZIONE</h4>
                    <h6>Parola D'ordine: Progettare</h6>
                </div>
                <p>
                    Un progetto studiamo nei minimi dettagli è la chiave di ogni risultato.
                </p>
            </div>
        </div>
        <div class="one-third">
            <div class="feature-block">
                <div class="feature-block-title">
                    <div class="feature-block-icon">
                        <i class="icon-desktop"></i><span></span>
                    </div>
                    <h4>MATERIALI</h4>
                    <h6>Materiali di prima scelta</h6>
                </div>
                <p>
                   I materiali e i prodotti vengono scelti in base alle esigenze e disponibilità del Cliente.
                </p>
            </div>
        </div>
        <div class="one-third">
            <div class="feature-block">
                <div class="feature-block-title">
                    <div class="feature-block-icon">
                        <i class="icon-cog"></i><span></span>
                    </div>
                    <h4>POSA IN OPERA</h4>
                    <h6>I nostri addetti curano ogni dettaglio</h6>
                </div>
                <p>
                    Ogni progetto richiede attenzione e professionalità: la posa in opera è il nostro punto di forza!
                </p>
            </div>
        </div><!--features block ends-->
        </div>
        <div class="intro-features"><!-- intro features panel starts -->
            <div class="container">
                <h4>PRODOTTI IN EVIDENZA</h4>
                <div class="slidewrap">
                    <!--project carousel starts-->

                    <ul class="slider" id="sliderName">
                        <li class="slide"><!-- carousel item starts -->
                            @foreach($products as $product)
                            <div class="one-fourth">
                                <div class="item-wrapp">
                                    <div class="portfolio-item">
                                        @if($product->images->count() > 0)
                                            <a href="{{ route('Prodotto', ['slug' => $product->slug]) }}" class="item-permalink"><i class="icon-link"></i></a>
                                            <a href="{{ $product->images[0]->image }}" data-rel="prettyPhoto" class="item-preview"><i class="icon-zoom-in"></i></a>
                                            <img src="{{ $product->images[0]->image }}" alt=""/>

                                        @else
                                            <img src="" alt=""/>
                                         @endif
                                    </div>
                                    <div class="portfolio-item-title">
                                        <a href="{{ route('ListaProdotti', ['slug' => $product->category->slug ]) }}">{{ $product->category->name }}</a>
                                        <p>
                                            <a href="{{ route('Prodotto', ['slug' => $product->slug]) }}">{{ $product->name }}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </li>

                    </ul><!-- carousel items UL ends -->

            </div></div>
    </div></div>
    <!-- intro features panel ends -->
			<div class=" clients-wrapp">
				<div class="container">
					<div class="flexslider clients-slider "><!-- flex slider starts -->
						<ul class="slides client-block">
							<li>
							<div class="one-fifth">
								<img src="images/fornitori/logo-cerrato.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-cocif.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-doors-project.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-ninz.jpg" alt=""/>
							</div>
							<div class="one-fifth ">
								<img src="images/fornitori/logo-steel-project.jpg" alt=""/>
							</div>
							</li>
							<li>
							<div class="one-fifth">
								<img src="images/fornitori/logo-cerrato.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-cocif.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-doors-project.jpg" alt=""/>
							</div>
							<div class="one-fifth">
								<img src="images/fornitori/logo-ninz.jpg" alt=""/>
							</div>
							<div class="one-fifth ">
								<img src="images/fornitori/logo-steel-project.jpg" alt=""/>
							</div>
							</li>
						</ul>
					</div><!-- flex slider ends -->
				</div>
			</div>
		</div>
		<section class="footer-call-to-action"><!-- footer call to action starts -->
		<section class="container">
		<div class="three-fourth">
			<h4>I migliori prezzi, i materiali più innovativi, le soluzioni più adatte: Studio PRE ti aspetta !<p>&nbsp;</p></h4>
		</div>
		</section>
		</section><!-- footer call to action ends -->
 @endsection

