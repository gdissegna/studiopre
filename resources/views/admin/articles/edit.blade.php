@extends('admin.master')


@section('content')
    <h1>Aggiungi Immagine</h1>
    <p>Aggiungi una Immagine alla slider Formato: jpg,png</p>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['url' => 'admin/articoli/{{ $article->id }}/edit','role' => 'form','files'=>true ]) !!}
            <div class="form-group">
                <label>Titolo Articolo</label>
                {!! Form::text('titolo',  $article->title  , ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Inserisci L'alt Text</label>
                {!! Form::textarea('body',  $article->body , ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label>Aggiungi le immagini</label>
                {!! Form::file('images[]', array('multiple'=>true)) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection