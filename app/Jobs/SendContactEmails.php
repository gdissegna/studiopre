<?php

namespace App\Jobs;

use Mail;
use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendContactEmails extends Job implements SelfHandling
{
    use SerializesModels;

    private $request;
    private $category;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request,$category)
    {
        $this->request = $request;
        $this->category = $category;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {

        $catname = $this->category->name;

        Mail::send('emails.contactform', ['request' => $this->request,'catname' => $catname], function($message)  {
            $message->from('info@studiopre.com', 'studio Pre');
            $message->to($this->request['email'], $this->request['name'] . $this->request['cognome'] );
            $message->subject('STUDIO PRE, Grazie per averci contatto');

        });

        Mail::send('emails.contactforminternal', ['request' => $this->request,'catname' => $catname], function($message) {
            $message->from('info@studiopre.com', 'studio Pre website');
            $message->to('info@studiopre.com');
            $message->subject('STUDIO PRE nuovo contatto');
        });

    }
}
