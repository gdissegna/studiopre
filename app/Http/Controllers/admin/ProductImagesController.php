<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Product;
use App\ProductImage;
use App\Http\Requests;
use App\Http\Requests\ProductImageCreateRequest;
use App\Http\Controllers\Controller;

class ProductImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($slug)
    {
        $product = Product::with('images')->where('slug', $slug)->first();

        return view('admin.products.productimages')->with('product', $product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $slug
     * @param ProductImageCreateRequest $request
     * @return Response
     */
    public function create($slug)
    {
        $product = Product::with('images')->where('slug', $slug)->first();

        return view('admin.products.productimagescreate')->with('product', $product);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store($slug ,ProductImageCreateREquest $request)
    {
        $product = Product::findBySlug($slug);

        $image = $request->all();

        $imagename = $image['image']->getClientOriginalName();
        $path = public_path('images/products/') . date('Y-m-d-h-i-s') . $imagename;
        $showpath = 'images/products/' . date('Y-m-d-h-i-s') . $imagename;
        \Image::make($image['image'])->resize(1100, null)->resizeCanvas(null, 750)->save($path);

        $newimage = new ProductImage();
        $newimage->filename = $imagename;
        $newimage->alttext = $image['alttext'];
        $newimage->image = $showpath;

        $product->Images()->save($newimage);


        return redirect()->route('productimages', [$product->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  string $slug
     * @return Response
     */
    public function destroy($slug,$id)
    {
        $image = ProductImage::find($id);

        $image->delete();

        $product = Product::findBySlug($slug);

        return redirect()->route('productimages', [$product->slug]);
    }
}
