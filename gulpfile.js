var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less('app.less');
});

elixir(function(mix) {
    mix.styles([
        "bootstrap.min.css",
        "font-awesome.min.css",
        "templatemo_main.css"
    ], 'public/css/admin.css');
});


elixir(function(mix) {
    mix.scripts([
        "jquery.min.js",
        "bootstrap.min.js",
        "Chart.min.js",
        "templatemo_script.js"
    ], 'public/js/.admin.js');
});