@extends('admin.master')

@section('content')
    <h1>Gestione Prodotti</h1>
    <p>Aggiungi e cancella Immagni Prodotto</p>

    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right" id="templatemo_sort_btn">
                <a href="{{ route('productimagescreate', ['slug' => $product->slug]) }}" type="button"  class="btn btn-default">Aggiungi nuova Immagine</a>
            </div>
            <div class="table-responsive">
                <h4 class="margin-bottom-15">Tabella Immagini Prodotto</h4>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Image</th>
                        <th>alt</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product->images as $image)
                        <tr>
                            <td>{{ $image->id }}</td>
                            <td class="col-md-2">{!! Html::image($image->image ,'', array('class' => 'img-responsive', 'width' => '300' )) !!}</td>
                            <td>{{ $image->alttext }}</td>
                            <td><a href="{{ route('productimagesdelete',['slug' => $product->slug, 'id' => $image->id ]) }}" class="btn btn-default">Delete</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

@endsection