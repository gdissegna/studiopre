@extends('master')

@section('meta')
    @parent
@endsection

@section('content')
    <div id="layerslider">
        <!--layer slider starts-->
        <div class="slider-shadow-top">
        </div>
        <div class="slider-shadow-bottom">
        </div>
        @foreach($sliders as $slider)
            <div class="ls-layer" style="slidedirection: top; slidedelay: 6000; durationin: 1500; durationout: 1500; delayout: 500;">
                <img src="{{ asset($slider->image) }}" class="ls-bg" alt="{{ $slider->alttext }}">
            </div>
        @endforeach
    </div>
    <div id="breadcrumb"><!-- breadcrumb starts-->
        <div class="container">
            <div class="one-half">
                <h4>Prodotti</h4>
            </div>
            <div class="one-half">
                <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                    <ul>
                        <li>Sei qui:</li>
                        <li><a href="/">Home</a></li>
                        <li><a href="/prodotti">Catalogo prodotti</a></li>
                    </ul>
                </nav>
                <!--breadcrumb nav ends -->
            </div>
        </div>
    </div>

    <div class="container">
        <div class="one-sixth sidebar left">
            @foreach($menu_categories as $menu_category)
                <div class="widget">
                    <h4 class="widget-title">{{ $menu_category->name }}</h4>
                    <ul class="sidebar-nav">
                        @foreach ($menu_category->children as $children)
                            <li><a href="{{ route('ListaProdotti', ['slug' => $children->slug ]) }}"
                                   title="">{{ $children->name }}</a></li>
                        @endforeach
                    </ul>

                </div>
            @endforeach
        </div>
        <div  class="three-fourth">

            <h4 class="category_top">{{ $category->name }}</h4>
            @foreach($products as $product)
            <div class="one-third">
                <div class="item-wrapp clearfix">
                    <div class="portfolio-item">
                        <a href="{{ route('Prodotto',['slug' => $product->slug ]) }}" class="item-permalink"><i class="icon-link"></i></a>
                        <a href="{{ asset($product->images[0]->image)  }}" data-rel="prettyPhoto" class="item-preview"><i class="icon-zoom-in"></i></a>
                        <img src="{{ asset($product->images[0]->image)  }}" alt=""/>
                    </div>
                    <div class="portfolio-item-title">
                        <a href="{{ route('Prodotto',['slug' => $product->slug ]) }}">{{ $product->name }}</a>

                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>


@endsection