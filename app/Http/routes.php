<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',['as' => 'index','uses' => 'IndexController@index']);

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');


Route::group(['middleware' => 'auth'], function()
{
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');

    Route::get('auth/adduser', 'Auth\NewUserController@getNewUser');
    Route::post('auth/adduser', 'Auth\NewUserController@postNewUser');
    // Slider Routes
    Route::get('admin',['as' => 'admin', 'uses' => 'admin\SliderController@index']);
    Route::get('admin/slider/create',[ 'as' => 'SliderCreate' , 'uses' => 'admin\SliderController@create']);
    Route::post('admin/slider/create',[ 'as' => 'SliderCreatePost' , 'uses' => 'admin\SliderController@store']);
    Route::get('admin/slider/{id}/edit',[ 'as' => 'SliderEdit', 'uses' => 'admin\SliderController@edit']);
    Route::post('admin/slider/{id}/edit',[ 'as' => 'SliderEditPost', 'uses' => 'admin\SliderController@update']);
    Route::get('admin/slider/{id}/delete',['as' => 'SliderDelete', 'uses' => 'admin\SliderController@destroy']);

    // Routes Articoli
    Route::get('admin/articoli', ['as' => 'articles', 'uses' => 'admin\ArticlesController@index']);
    Route::get('admin/articoli/create',[ 'as' => 'ArticleCreate', 'uses' => 'admin\ArticlesController@create']);
    Route::post('admin/articoli/create',[ 'as' => 'ArticleCreatePost', 'uses' => 'admin\ArticlesController@store']);
    Route::get('admin/articoli/{id}/edit',['as' => 'ArticleEdit', 'uses' => 'admin\ArticlesController@edit']);
    Route::post('admin/articoli/{id}/edit',['as' => 'ArticleEditPost', 'uses' => 'admin\ArticlesController@update']);
    Route::get('admin/articoli/{id}/delete',['as' => 'ArticleDelete', 'uses' => 'admin\ArticlesController@destroy']);

    // Routes Categorie
    Route::get('admin/categorie', ['as' => 'categories', 'uses' => 'admin\CategoriesController@index']);
    Route::get('admin/categorie/create', ['as' => 'CategoryCreate', 'uses' => 'admin\CategoriesController@create']);
    Route::post('admin/categorie/create', ['as' => 'CategoryCreatePost', 'uses' => 'admin\CategoriesController@store']);
    Route::get('admin/categorie/{id}/edit',['as' => 'CategoryEdit', 'uses' => 'admin\CategoriesController@edit']);
    Route::post('admin/categorie/{id}/edit',['as' => 'CategoryEditPost', 'uses' => 'admin\CategoriesController@update']);
    Route::get('admin/categorie/{id}/delete',['as' => 'CategoryDelete', 'uses' => 'admin\CategoriesController@destroy']);

    Route::get('admin/prodotti',['as' => 'products', 'uses' => 'admin\ProductsController@index']);
    Route::get('admin/prodotti/create',['as' => 'ProductCreate' , 'uses' => 'admin\ProductsController@create']);
    Route::post('admin/prodotti/create',['as' => 'ProductCreatePost' , 'uses' => 'admin\ProductsController@store']);
    Route::get('admin/prodotti/{id}/edit',['as' => 'ProductEdit', 'uses' => 'admin\ProductsController@edit']);
    Route::post('admin/prodotti/{id}/edit',['as' => 'ProductEditPost', 'uses' => 'admin\ProductsController@update']);
    Route::get('admin/prodotti/{id}/delete',['as' => 'ProductDelete', 'uses' => 'admin\ProductsController@destroy']);

    Route::get('admin/prodotti/{slug}/images',['as'=> 'productimages' ,'uses' =>'admin\ProductImagesController@index']);
    Route::get('admin/prodotti/{slug}/images/create',['as' =>'productimagescreate','uses' =>'admin\ProductImagesController@create']);
    Route::post('admin/prodotti/{slug}/images/create',['as' => 'productimagescreatepost','uses' =>'admin\ProductImagesController@store']);
    Route::get('admin/prodotti/{slug}/images/{id}/edit',['as' =>'productimagesedit','uses' =>'admin\ProductImagesController@edit']);
    Route::post('admin/prodotti/{slug}/imagesimages/{id}/edit',['as' => 'producimageseditpost','uses' =>'admin\ProductImagesController@update']);
    Route::get('admin/prodotti/{slug}/images/{id}/delete',['as' => 'productimagesdelete','uses' =>'admin\ProductImagesController@destroy' ]);
    // PDF dovnload

});

    Route::get('/',['as' => 'index', 'uses' => 'IndexController@index']);
    Route::get('/prodotti', ['as' => 'categorie', 'uses' => 'CategoriesController@CategoriesPage']);
    Route::get('/prodotti/{slug}/lista', ['as' => 'ListaProdotti', 'uses' => 'ProductsController@ProductList' ]);
    Route::get('/prodotti/{slug}',['as' => 'Prodotto', 'uses' => 'ProductsController@SingleProduct']);
    Route::get('/prodotti/{slug}/download',['as' => 'downloads','uses' =>'ProductsController@download']);
    Route::get('/contatti', ['as' => 'contatti', 'uses' => 'ContactsFormController@index']);
    Route::post('/contatti', ['as' =>'contattisubmit', 'uses' => 'ContactsFormController@submittedform']);

    // Cart Routes
    Route::get('/carrello','CartController@showcartform');
    Route::post('/carrello','CartController@postcartform');
    Route::get('/cartadd/{id}',['as'=>'cartadditem','uses' => 'CartController@additem']);
    Route::get('/cartdelete/{id}',['as'=>'cartremoveitem','uses' => 'CartController@removeitem']);

    // Static Pages
    Route::get('/servizi-chiavi-in-mano',['as' => 'ChiaviInMano', 'uses' => 'StaticPagesController@chiaviinmano']);
    Route::get('/note-legali',['as' => 'NoteLegali', 'uses' => 'StaticPagesController@notelegali']);
    Route::get('/lo-studio',['as' => 'ChiSiamo', 'uses' => 'StaticPagesController@chisiamo']);
    Route::get('/privacy',['as' => 'Privacy', 'uses' => 'StaticPagesController@privacy']);