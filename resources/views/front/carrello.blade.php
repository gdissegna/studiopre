@extends('master')

@section('meta')
    @parent
@endsection

@section('content')

    <div id="content" style="margin-bottom: 25px">
        <div id="breadcrumb"><!-- breadcrumb starts-->
            <div class="container">
                <div class="one-half">
                    <h4>Contatti</h4>
                </div>
                <div class="one-half">
                    <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                        <ul>
                            <li>You are here:</li>
                            <li><a href="/">Home</a></li>
                            <li><a href="/contatti">contatti</a></li>
                        </ul>
                    </nav>
                    <!--breadcrumb nav ends -->
                </div>
            </div>
        </div>
        <!--breadcrumbs ends -->
        <div class="container">

            <div class="one">
                <h1>Carrello</h1>
                <br>
                <h3>Prodotti presenti nel Carrello</h3>
                <ul style="margin-bottom: 15px;">
                    @foreach($cartitems as $item)
                    <li><span style="font-size: 1.2em;"> {{ $item->name }}</span> <a id="cart-prodotto" href="{{ route('cartremoveitem', ['id' => $item->id ]) }}" style="padding: 5px;"  class="button color-alt">rimuovi</a></li>
                    @endforeach
                </ul>
                <hr>
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <div id="error-field">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                {!! Form::open(['contatti' => 'contatti','url' => '/carrello', 'method' => 'post','class' => 'simple-form']) !!}

                <label>Nome*</label>
                <fieldset>
                    {!! Form::text('nome', 'Nome', ['class' => 'form-control']) !!}
                </fieldset>

                <label>Cognome*</label>
                <fieldset>
                    {!! Form::text('cognome', 'Cognome', ['class' => 'form-control']) !!}
                </fieldset>

                <label>Azienda</label>
                <fieldset>
                    {!! Form::text('azienda', 'Azienda', ['class' => 'form-control']) !!}
                </fieldset>

                <label>Email*</label>
                <fieldset>
                    {!! Form::text('email', 'Email', ['class' => 'form-control']) !!}
                </fieldset>

                <label>Indirizzo</label>
                <fieldset>
                    {!! Form::text('Indirizzo', 'Indirizzo', ['class' => 'form-control']) !!}
                </fieldset>
                <label>Citta</label>
                <fieldset>
                    {!! Form::text('citta', 'Citta', ['class' => 'form-control']) !!}
                </fieldset>
                <label>Provincia</label>
                <fieldset>
                    {!! Form::text('provincia', 'Provincia', ['class' => 'form-control']) !!}
                </fieldset>
                <fieldset>
                    {!! Form::textarea('messaggio', 'Messaggio',['class' =>'form-control']) !!}
                </fieldset>
                <br>
                {!! Recaptcha::render() !!}
                <br>
                <div style="margin-top: 250px"></div>
                {!! Form::submit('Fatto!', ['class' => 'button huge color-alt']) !!}

                {!! Form::close() !!}
            </div>

        </div>

    </div>


@endsection